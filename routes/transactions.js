let express = require('express');
let router = express.Router();
let mongojs = require('mongojs');
let db = mongojs('mongodb://otalan:ghbdtn1991@ds129770.mlab.com:29770/thehouse_db', ['transactions']);

// Get all transactions
router.get('/transactions', function(req, res, next) {
    db.transactions.find(function(err, transactions) {
        if (err) {
            res.send(err);
        }
        res.json(transactions);
    })
});

// Save Transaction
router.post('/transaction', function(req, res, next) {
    let transaction = req.body;
    if ((!transaction.value) || (!transaction.paid_by) || (!transaction.is_shared)) {
        res.status(400);
        res.json({
            "error": "Bad Data"
        });
    } else {
        db.transaction.save(task, function(err, transaction) {
            if (err) {
                res.send(err);
            }
            res.json(transaction);
        });
    }
});

module.exports = router;