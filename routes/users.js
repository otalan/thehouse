let express = require('express');
let router = express.Router();
let mongojs = require('mongojs');
let db = mongojs('mongodb://otalan:ghbdtn1991@ds129770.mlab.com:29770/thehouse_db', ['users']);
let ObjectId = require('mongodb').ObjectID;

// Get all users
router.get('/users', function(req, res, next) {
    db.users.find(function(err, users) {
        if (err) {
            res.send(err);
        }
        res.json(users);
    })
});

// Get user by ID
router.get('/user/id', function(req, res, next) {
    db.users.findOne({
        _id: mongojs.ObjectId(req.params.id)
    }, function(err, user) {
        if (err) {
            res.send(err);
        }
        res.json(users);
    })
});

// Save user
router.post('/user', function(req, res, next) {
    let user = req.body;
    if (!user.name) {
        res.status(400);
        res.json({
            "error": "Bad Data"
        });
    } else {
        if (!user.balance) {
            user.balance = 0;
        }
        db.users.save(user, function(err, user) {
            if (err) {
                res.send(err);
            }
            res.json(user);
        });
    }
});

router.delete('/user/:id', function(req, res, next) {
    console.log(req.params)
    console.log(req.params.id)
    db.users.remove({
        "_id": ObjectId(req.params.id)
    }, (err, data) => {
        console.log(data)
        if (err) {
            res.send(err);
            console.error('error deleting user ->', err);
            return res.send(err);
        }
        console.log('success deleting user');
        return res.send('');
        // req.logout();
        // return res.redirect('/home')
    })
});

// Update user
//
//
///////////////    Consider way to update user balance due to transaction /////////////////
//
//
//

module.exports = router;
