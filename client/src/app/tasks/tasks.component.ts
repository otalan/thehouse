import { Component, OnInit } from '@angular/core';
import { TaskService } from "../shared/services/task.service";
import { Task } from "../shared/models/task.model";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  tasks: Task[];
  title: string;

  constructor(
    private taskService: TaskService
  ) { }

  ngOnInit() {
    this.fetchData();
  }

  fetchData(): void {
    console.log('Fetching data...');
    this.taskService.getTasks().subscribe(tasks => {
      this.tasks = tasks;
      console.log(this.tasks);
    });
  }

  addTask(e): void {
    e.preventDefault();
    console.log(this.title);
    let newTask = {
      title: this.title,
      isDone: false
    };
    this.taskService.addTask(newTask).subscribe(err => {
      if (!err) {
        this.fetchData();
        this.title = '';
      }
    })
  }

  deleteTask(taskId): void {
    console.log(taskId);
    this.taskService.deleteTask(taskId).subscribe(err => {
      console.log(err)
      if (!err) {
        this.fetchData();
      }
    })
  }
}
