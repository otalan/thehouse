import { Injectable } from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from "@angular/common/http";
import { Task } from "../models/task.model";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { of } from "rxjs/internal/observable/of";
import {User} from "../models/user.model";

@Injectable()

export class UserService {

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get<User[]>('http://localhost:3000/api/users');
  }

  addUser(user): Observable<HttpResponse<User>> {
    return this.http.post<User>('http://localhost:3000/api/user', user, {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        observe: 'response'
      }).pipe(
          map(res => console.log('Add User succesfull', res)),
          catchError(err => of('Add user failed with following error:', err))
      );
  }

  removeUser(id: string): Observable<HttpResponse<boolean>> {
    return this.http.delete<User>(`http://localhost:3000/api/user/${id}`, {observe: 'response'}).pipe(
        map(res => console.log('Remove User successful', res)),
        catchError(err => of('Remove User failed with error -> ', err))
      );
  }

  errorHandler(error: any): void {
    console.log(error)
  }
}
