import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Task } from "../models/task.model";
import { Observable } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { of } from "rxjs/internal/observable/of";

@Injectable()
export class TaskService {
  constructor(private http: HttpClient) { }

  getTasks() {
    return this.http.get<Task[]>('http://localhost:3000/api/tasks');
  }

  addTask(newTask): Observable<HttpResponse<Task>> {
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<Task>('http://localhost:3000/api/task', newTask, { headers: headers, observe: 'response' })
      .pipe(
        map(res => console.log('Add Task succesfull', res)),
        catchError(err => of('Add task failed with following error:', err))
      );
  }

  deleteTask(taskId): Observable<HttpResponse<Task>> {
    return this.http.delete('http://localhost:3000/api/task/'+taskId)
      .pipe(
        map(res => console.log('Delete Task succesfull', res)),
        catchError(err => of('Delete task failed with following error:', err))
      );
  }

  errorHandler(error: any): void {
    console.log(error)
  }
}
