import { Component, OnInit } from '@angular/core';
import {UserService} from "../shared/services/user.service";
import {User} from "../shared/models/user.model";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[];
  username: string;

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.fetchData();
  }

  fetchData(): void {
    console.log('Fetching data...');
    this.userService.getUsers().subscribe(users => {
      this.users = users;
      console.log(this.users);
    });
  }

  addUser(e): void {
    e.preventDefault();
    console.log(this.username);
    let newUser = {
      name: this.username,
    };
    this.userService.addUser(newUser).subscribe(err => {
      if (!err) {
        this.fetchData();
        this.username = '';
      }
    })
  }

  removeUser(user, e?: any): void {
    e.preventDefault();
    console.log(user);
    console.log(user._id);
    this.userService.removeUser(user._id).subscribe(err => {
      console.error('ERROR REMOVING USER ->', err)
      if (!err) {
        this.fetchData();
      }
    });
  }

}
