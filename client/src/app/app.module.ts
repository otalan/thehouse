import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule
} from "@angular/material";
import { TasksComponent } from './tasks/tasks.component';
import { TaskService } from "./shared/services/task.service";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { UserService } from "./shared/services/user.service";
import { UsersComponent } from "./users/users.component";
import { RouterModule, Routes } from "@angular/router";
import {ROUTER_PROVIDERS} from "@angular/router/src/router_module";
import {HashLocationStrategy, LocationStrategy} from "@angular/common";

const routes: Routes = [
  { path: 'admin', component: UsersComponent },
  { path: 'tasks', component: TasksComponent },
  { path: '', redirectTo: '/tasks', pathMatch: 'full' }
];

@NgModule({
    declarations: [
      AppComponent,
      TasksComponent,
      UsersComponent
    ],
    imports: [
        HttpClientModule,
        BrowserModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatInputModule,
        MatFormFieldModule,
        MatIconModule,
        RouterModule.forRoot(routes, { enableTracing: true, useHash: true }),
        FormsModule
    ],
    providers: [
      TaskService,
      UserService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
